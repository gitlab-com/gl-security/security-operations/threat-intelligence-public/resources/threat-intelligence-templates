## Key Takeaways

- ...
- ...
- ...

## Recommendations

- ...
- ...

## Technical Details

<details>

<summary>

### Threat Analysis

</summary>

[long-form narrative of the threat]

</details>

<details>

<summary>

### Indicators of Compromise (IoCs)

</summary>

- ...
- ...
- ...

</details>

## References

- ...
- ...
