# Threat Intelligence - Request For Information

## Request Details

> _Provide a clear description of your request, including how you intend to use the information. Let us know what you would consider to be a specific measurable indicator that the request has been successfully fulfilled._

## Priority / Deadline

> _Specify the priority level of the request (e.g., High, Medium, Low) and provide a brief justification._

## Data Sensitivity and Sharing Restrictions

> _Let us know if the information can be delivered via this issue, or if it needs to remain restricted to certain groups/people._

## Additional Resources

> _Add links to any related incidents, issues, etc._

/label ~"TIWork:RFI"
