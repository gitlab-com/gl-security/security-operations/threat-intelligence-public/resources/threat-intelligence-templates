> _This report covers the period DD MM to DD MM YYYY and includes a recap of Threat Intelligence Flash Reports and other industry news relevant to GitLab._

<details>
<summary>Table of Contents</summary>

[[_TOC_]]

</details>

## Key Takeaways

### Flash Report Outcomes

- ...
- ...
- ...

### Industry News

- ...
- ...
- ...

## Flash Reports

<details>
<summary>

### Flash Report X

</summary>

> _(Short form recap of the Flash Report including any key information that emerged during subsequent to publication.)_

#### Action Taken

> _(Narrative of the recommendations made and their progress, referring specifically to the actioning team.)_

</details>

## Industry news relevant to GitLab

<details>
<summary>

### Top threat actor updates

</summary>

#### Threat Actor X

> _(Short form (one paragraph) summary of the industry reporting followed by an analyst comment calling out special relevance or thematic links if appropriate.)_

</details>

<details>
<summary>

### Threat landscape updates

</summary>

#### Landscape Update X

> _(Short form (one paragraph) summary of the industry reporting followed by an analyst comment calling out special relevance or thematic links if appropriate.)_

</details>
