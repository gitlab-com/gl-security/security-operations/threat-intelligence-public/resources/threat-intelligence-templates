# Threat Intelligence Templates

This project contains templates used by GitLab's Threat Intelligence team.

We use these for producing reports and accepting RFIs. A template repository helps ensure consistency while promoting transparency and collaboration.

We don't do any actual work in this project - we have a private mirror that allows us to leverage the templates securely. You can fork this project yourself, and then use the templates when creating new issues.

## Contributing

Feel free to open an issue or a merge request. Because we use these templates ourselves, we may choose not to accept changes that don't quite fit our own needs - even if they make sense elsewhere.

Open an issue first to discuss if you are unsure whether to pursue changes that may not be accepted.
